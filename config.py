import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'yvoma@986635221454azycbnde'
    __host = os.environ["PGHOST"] if "PGHOST" in os.environ else "127.0.0.1"
    __port = os.environ["PGPORT"] if "PGPORT" in os.environ else "5432"

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        "postgresql://fbapp:fbapp@" + __host + ":" + __port + "/fbapp"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
